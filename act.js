//CREATE
db.users.insertMany([
{
	firstName: "Buelly",
	lastName: "Buendea",
	email: "buellybuenda@gmail.com",
	password:"buellybuenda123",
	isAdmin: false
},
{
	firstName: "Elly",
	lastName: "Lerio",
	email: "ellylorio@gmail.com",
	password:"ellylorio123",
	isAdmin: false
},
{
	firstName: "Jay",
	lastName: "Manalo",
	email: "jaymanalo@gmail.com",
	password:"jay123",
	isAdmin: false
},
{
	firstName: "kean",
	lastName: "cipriano",
	email: "ciancipriano@gmail.com",
	password:"cian123",
	isAdmin: false
},
{
	firstName: "Rock",
	lastName: "Star",
	email: "rockstar@gmail.com",
	password:"rock123",
	isAdmin: false
},
])

db.courses.insertMany([
{
	name:"HTML crash course",
	price: 500,
	isActive: false

},
{
	name:"CSS crash course",
	price: 700,
	isActive: false
},
{

	name:"JAVASCRIPT crash course",
	price: 1200,
	isActive: false
}
])



//READ
db.users.find({isAdmin: false})


//UPDATE
db.users.updateOne({}, {$set: {isAdmin: true}})
db.courses.updateOne({name:"JAVASCRIPT crash course"},
	{$set: {isActive: true}})

//DELETE
db.courses.deleteMany({isActive: false})