db.products.insertMany([
{
    name: "Iphone X", 
    price: 30000,
    isActive : true
},

{
    name: "Samsung Galaxy S21", 
    price: 51000,
    isActive : true
},

{
    name: "Razer Blackshark V2X", 
    price: 2800,
    isActive : false
},

{
    name: "RAKK Gaming Mouse", 
    price: 1800,
    isActive : true
},

{
    name: "Razer Mechanical Keyboard", 
    price: 4000,
    isActive : true
}
])

// IF(TRUE)
// {

//  STATEMENT
// }



// ------------------------------------------- QUERY OPERATOR
//QUERY OPERATOR ALLOW FOR MORE FLEXIBLE QUERYING IN MONGODB
//INSTEAD OF JUST HAVING TO FIND/SEARCH FOR DOCUMENTS WITH EXACT AND DEFINITE VALUE
// WE COULD USE QUERY OPERATORS TO DEFINE CONDTIONS INSTEAD OF JUST SPECIFIC CREITERIA AND VALUE
//$gt, $lt, $gte, $lte

//$gt - greater than query operator
    //find all items in the products collection whose price is greather than 3000

//if(price > 3000)
db.products.find({price: {$gt:3000}})
//if(price > 10000)
db.products.find({price: {$gt:10000}})

// $lt - less than query operator
//if(price < 4000)
db.products.find({price: {$lt:4000}})
//if(price < 10000)
db.products.find({price: {$lt:10000}})

//$gte - greater than or equal query operator

//if(price <= 4000)
db.products.find({price: {$gte:4000}})
//if(price <= 2000)
db.products.find({price: {$gte:2000}})

// $lte - less than or equal to operator

//if(price >= 4000)
db.products.find({price: {$lte:4000}})
//if(price >= 30000)
db.products.find({price: {$lte:30000}})

//Query operators can also be used to expand queries when deleting or updating.

                    /*  if(price <= 30000){
                            isActive = false
                    }
                        if(condition){
                            statement
                        }

                    // db.products.updateMany({condition},{statement})
                    */
db.products.updateMany({price: {$gte:30000}}, {$set:{isActive:false}})

/*
    Mini Activity



*/
db.users.insertMany([
{
    "firstname": "Mary jane",
    "lastname": "Watson",
    "email": "mjtiger@gmail.com",
    "password": "tigerjackpot15",
    "isAdmin": false

},

{
    "firstname": "Gwen",
    "lastname": "Stacy",
    "email": "stacyTech@gmail.com",
    "password": "stacyTech1991",
    "isAdmin": true

},

{
    "firstname": "Peter",
    "lastname": "Parker",
    "email": "peterWebDev@gmail.com",
    "password": "webdeveloperPeter",
    "isAdmin": true

},

{
    "firstname": "Jonah",
    "lastname": "Jameson",
    "email": "jjjameson@gmail.com",
    "password": "spideyisamenace",
    "isAdmin": false

},

{
    "firstname": "Otto",
    "lastname": "Octavius",
    "email": "ottoOctopi@gmail.com",
    "password": "docOck15",
    "isAdmin": true
},

])



//------------------------------------------- $regex
//this query operator will allow us to match/find documents which will match the pattern/characters that we are looking for.
//NOTE: $regex is case sensitive
        
                /*
                    if(firstname === "o"){
                            statement
                    }
                */
db.users.find({firstname:{$regex:'O'}})
db.users.find({firstname:{$regex:'o'}})

//$options: '$i'- used to make our $regex non-case sensitive.
                    /*
                        if(email === "T" || firstname === "t"){
                                statement
                        }
                    */
db.users.find({firstname:{$regex:'O', $options: '$i'}})

//We can also use $regex to find for documents which matches a specific pattern or word in a field.

                    /*
                        if(email)

                    */
db.users.find({email:{$regex:'web', $options: '$i'}})
db.products.find({name:{$regex:'phone', $options: '$i'}})

//
/*
    Mini Activity

*/

db.products.find({name:{$regex:'razer', $options: '$i'}})

db.products.find({name:{$regex:'rakk', $options: '$i'}})


//------------------------------------------- $or and $and
//$or operator - || - allows us to have a logical operation wherein we can look or find for documents which can satisify at least one of our conditions.

/*
    if(email)
*/

db.products.find({$or:
    [

        {name:{$regex:'x',$options:'$i'}},
        {price:{$lt:10000}},

    ]})


db.products.find({$or:[{name:{$regex:'x',$options:'$i'}}, {price:30000}]})
db.users.find({$or:[{firstname:{$regex:'a',$options:'$i'}}, {isAdmin:true}]})
db.users.find({$or:[{firstname:{$regex:'e',$options:'$i'}}, {isAdmin:true}]})


//$and operator - && allows us to have a logical operation wherein we can look or find for documents whicn can satistfy all conditions

db.products.find({$and:[{name:{$regex:'razer',$options:'$i'}}, {price:{$gte:3000}}]})

db.users.find({
    $and:[
    {firstname:{$regex:'e',$options:'$i'}}, 
    {lastname:{$regex:'a',$options:'$i'}}
]})

/*
    if(price > 3000 && price < 50000)
*/
db.products.find({$and:[{price:{$gt:3000}}, {price:{$lt:50000}}]})

//Field Projection
    //find() actually can have 2 arguments, the search criteria and the projection.
    //inclusion or exclusion of fields in the returned documents
    //find({criteria}, {projection})


db.users.find({}, {"_id":0, "password":0})
    //In field projection, we can show/hide fields: 0 means hide, 1 means show
db.users.find({}, {"password":1})
    //In field projection, we can implicitly/automatically hide other fields by only showing or including the needed fields.
    //Except for the _id. The _id has to be explicitly hidden.
db.users.find({}, {"_id":0,"isAdmin":1})

db.users.find({}, {"firstname":1, "lastname":1, "email":1})
db.users.find({}, {"_id":0,"firstname":1, "lastname":1, "email":1})


//db.users.find({criteria}, {projection})
/*
    Mini Activity:
    Look for users whose isAdmin property is true but only show their email.

*/

db.users.find({"isAdmin":true}, {"_id":0, "email":1 })


// db.users.insertMany([
// {
//     "firstname": "Mary jane",
//     "lastname": "Watson",
//     "email": "mjtiger@gmail.com",
//     "password": "tigerjackpot15",
//     "isAdmin": false

// },

// {
//     "firstname": "Gwen",
//     "lastname": "Stacy",
//     "email": "stacyTech@gmail.com",
//     "password": "stacyTech1991",
//     "isAdmin": true

// },

// {
//     "firstname": "Peter",
//     "lastname": "Parker",
//     "email": "peterWebDev@gmail.com",
//     "password": "webdeveloperPeter",
//     "isAdmin": true

// },

// {
//     "firstname": "Jonah",
//     "lastname": "Jameson",
//     "email": "jjjameson@gmail.com",
//     "password": "spideyisamenace",
//     "isAdmin": false

// },

// {
//     "firstname": "Otto",
//     "lastname": "Octavius",
//     "email": "ottoOctopi@gmail.com",
//     "password": "docOck15",
//     "isAdmin": true
// },

// ])